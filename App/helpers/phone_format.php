<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');


if ( !function_exists('phone_format'))
{
    /**
     * Форматирует телефонный номера в международный вид и
     * проверяет номер телефона по базе данных, по моб. операторам,
     * и городским номерам.
     *
     * @param  string $phone  Входной номер телефона
     * @param  string $format По-умолчанию: +%d (%d) %s-%s-%s
     *                        Символ маски: %m
     * @param  string $mask   Маска (скрытие части номера). Символ скрытия цифр.
     * @return string
     */
    function phone_format($phone = '', $format = NULL, $mask = 'X')
    {
        $CI =& get_instance();
        $CI->load->library('phone_format');
        return $CI->phone_format->run($phone, $format, $mask);
    }
}

// -----------------------------------------------------------------------------
