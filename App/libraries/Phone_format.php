<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

/**
 * Extends CI_Controller
 *
 * @package     Sputnik
 * @subpackage  CodeIgniter
 * @category    Core
 * @author      Mahlenko Sergey <sm@weblive.by>
 * @link        http://sputnik.by/
 */

class Phone_format {


    /**
     * -----------------------------------------------------------
     * Международный код страны и коды города
     * --------------------------------------------------------- */
    private $dialcode = array();
    private $dialcode_full = array();

    /**
     * -----------------------------------------------------------
     * Для конвертации буквенного номера в цифровой
     * --------------------------------------------------------- */
    public $convert2int = array(
        '2'=>array('a','b','c'),
        '3'=>array('d','e','f'),
        '4'=>array('g','h','i'),
        '5'=>array('j','k','l'),
        '6'=>array('m','n','o'),
        '7'=>array('p','q','r','s'),
        '8'=>array('t','u','v'),
        '9'=>array('w','x','y','z')
        );


    // Get Instance
    private $CI;

    /**
     * ---------------------------------------------------------------
     * __construct
     * ---------------------------------------------------------------
     */
    public function __construct() {
        // parent::__construct();
        $this->CI =& get_instance();
        $this->_initialize();
    }


    /**
     * ---------------------------------------------------------------
     * Форматирование номера телефона
     * ---------------------------------------------------------------
     */
    public function run($phone = '', $format = '+%d (%d) %s-%s-%s', $mask = 'X') {
        $phone = preg_replace('/[^0-9a-z]/', '', $phone);

        if (!$this->is_correct($phone))
        {
            return FALSE;
        }

        if (empty($phone)) {
            return NULL;
        }

        if (!$format) {
            $format = '+%d (%d) %s-%s-%s';
        }

        // Конвертация буквенного номера в цифровой
        $this->_letter2int($phone);
        
        /**
         * -----------------------------------------------------------
         * Поиск маски
         * --------------------------------------------------------- */
        $mask_format = preg_replace('/[^%a-z]/', ' ', $format);
        foreach (explode('%', $mask_format) as $key => $value) {
            if (!trim($value)) continue;
            $_mask_format[] = trim($value);
        }

        $format = str_replace('%m', '%s', $format);


        /**
         * -----------------------------------------------------------
         * Поиск страны
         * --------------------------------------------------------- */
        foreach ($this->dialcode as $key => $value) {
            if (preg_match('/^('.$key.')/', $phone)) {

                $dialcode = $this->dialcode[$key];

                // Код страны
                $_phone[] = $key;
                $phone = substr($phone, strlen($key));

                // Код мобильного оператора
                preg_match_all('/^('.implode($dialcode['mobile'], '|').')/', $phone, $matches);
                if (isset($matches[0][0]) && intval($matches[0][0]) > 0) {
                    $_phone[] = $matches[0][0];
                }

                if (!isset($_phone[1])) {
                    // Код оператора стационарного номера
                    preg_match_all('/^('.implode($dialcode['cities'], '|').')/', $phone, $matches);
                    if (isset($matches[0][0]) && intval($matches[0][0]) > 0) {
                        $_phone[] = $matches[0][0];
                    }
                }

                if (!isset($_phone[1])) {
                    $min = strlen(min($dialcode['cities']));
                    $max = strlen(max($dialcode['cities']));
                    $sum = ceil(($min + $max)/2);
                    $_phone[] = substr($phone, 0, $sum);
                }

                $phone = substr($phone, strlen($_phone[1]));
                $phone_len = strlen($phone);

                /**
                 * -----------------------------------------------------------
                 * Format number
                 * --------------------------------------------------------- */
                $first_delimiter = $phone_len - 4;
                $_phone[] = substr($phone, 0, $first_delimiter);
                $_phone[] = substr($phone, $first_delimiter, 2);
                $_phone[] = substr($phone, $first_delimiter + 2, 2);

                /**
                 * -----------------------------------------------------------
                 * Маскируем вывод
                 * --------------------------------------------------------- */
                foreach ($_phone as $key => $value) {
                    if ($_mask_format[$key] == 'm') {
                        $_len = strlen($value);
                        $value = '';
                        for ($i=0; $i < $_len; $i++) {
                            $value .= $mask;
                        }
                        $_phone[$key] = $value;
                    }
                }

                break;
            }
        }

        if ( empty($_phone) ) {
            return $phone;
        }

        array_unshift($_phone, $format);

        $format_phone = call_user_func_array ('sprintf', $_phone);
        return $format_phone;
    }


    /**
     * Проверить корректность номера
     *
     * @param string $phone
     * @param string $type mobile/cities/false
     * @return bool
     */
    public function is_correct($phone = '', $type = 'mobile')
    {
        $phone_types = array('mobile', 'cities');

        if (empty($phone) and array_search($type, $phone_types) === FALSE)
        {
            return FALSE;
        }

        // Чистим телефонный номер от лишних символов
        $phone = preg_replace('/[^0-9a-z]/', '', $phone);
        
        // Список кодов стран
        $dialcodes = array_column($this->dialcode_full, 'dialcode');
        
        // Поиск страны к которой относится номер
        foreach($dialcodes as $code)
        {
            if(preg_match('/^'.$code.'/', $phone))
            {
                $dialcode = $code;
                break;
            }
        }
        
        if (!isset($dialcode))
        {
            return FALSE;
        }

        // Поиск кода оператора к которой относится номер
        if (isset($this->dialcode[$dialcode][$type])) {
            $operator_codes = $this->dialcode[$dialcode][$type];

            foreach ($operator_codes as $code)
            {
                if (preg_match('/^'.$dialcode . $code.'/', $phone))
                {
                    $operator = $code;
                    break;
                }
            }
        }

        // Кол-во знаков после кода,
        // обычно это 7 знаков - других данных не нашел :)
        if ( isset($operator) )
        {
            $intercode = $dialcode . $operator;
            $phone_number = substr($phone, strlen($intercode));

            if (strlen($phone_number) < 7)
            {
                return FALSE;
            }

            return TRUE;
        }

        return FALSE;
    }




    /**
     * ---------------------------------------------------------------
     * Конвертация буквенного номера в цифровой
     * ---------------------------------------------------------------
     */
    private function _letter2int(&$phone) {
        foreach($this->convert2int as $int => $letters) {
            $phone = str_replace($letters, $int, $phone);
        }

        return $phone;
    }



    /**
     * ---------------------------------------------------------------
     * Инициализация библиотеки
     * ---------------------------------------------------------------
     */
    private function _initialize() {

        if (count($this->dialcode) > 0) {
            return $this->dialcode;
        }


        /**
         * -----------------------------------------------------------
         * Получаем коды из базы
         * --------------------------------------------------------- */
        $this->CI->db->select('id, name, dialcode');
        $country = id2key($this->CI->db->get('geo_country')->result_array());
        foreach ($country as $key => $value) {
            unset($country[$key]['id']);
        }

        /**
         * -----------------------------------------------------------
         * Коды городов
         * --------------------------------------------------------- */
        $this->CI->db->select('id, country_id, name, dialcode');
        $this->CI->db->where('dialcode >', 0);
        $this->CI->db->group_by('dialcode');
        $this->CI->db->order_by('dialcode', 'DESC');
        $cities = $this->CI->db->get('geo_cities')->result_array();

        /**
         * -----------------------------------------------------------
         * Мобильные коды
         * --------------------------------------------------------- */
        $this->CI->db->order_by('dialcode', 'DESC');
        $mobile = $this->CI->db->get('dialcode_mobile')->result_array();
        foreach ($mobile as $item) {
            if (isset($country[$item['country_id']])) {
                $country[$item['country_id']]['mobile'][$item['id']] = $item;
                $country[$item['country_id']]['mobile_json'][$item['id']] = $item['dialcode'];
            }
        }

        /**
         * -----------------------------------------------------------
         * Собираем города с их странами
         * --------------------------------------------------------- */
        foreach ($cities as $item) {
            if (isset($country[$item['country_id']])) {
                $country[$item['country_id']]['citycode'][$item['id']] = $item;
                $country[$item['country_id']]['citycode_json'][$item['id']] = $item['dialcode'];
            }
        }

        $this->dialcode_full = $country;

        /**
         * -----------------------------------------------------------
         * Сохранить $this->dialcode
         * --------------------------------------------------------- */
        foreach ($country as $item) {
            $this->dialcode[$item['dialcode']] = array(
                'mobile' => $item['mobile_json'],
                'cities' => $item['citycode_json'],
                );
        }

        return;
    }




}